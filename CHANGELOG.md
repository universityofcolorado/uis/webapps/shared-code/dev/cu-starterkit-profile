# CU Drupal 10 Install Profile

All notable changes to this project will be documented in this file.

Repo : [GitLab Repository](https://gitlab.com/universityofcolorado/uis/webapps/shared-code/dev/cu-starterkit-profile)

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

